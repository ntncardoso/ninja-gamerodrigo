package org.academiadecodigo.rhashtafaris.projectninja;

import org.academiadecodigo.simplegraphics.mouse.Mouse;
import org.academiadecodigo.simplegraphics.mouse.MouseEvent;
import org.academiadecodigo.simplegraphics.mouse.MouseEventType;
import org.academiadecodigo.simplegraphics.mouse.MouseHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;

import java.sql.Time;
import java.util.Timer;
import java.util.TimerTask;

public class Ninja {

    private int health = 3;
    private int fallingSpeed = 5;
    private double mouseXCordinates;
    private double mouseYCordinates;
    private Picture ninjaStanding;
    private Picture ninjaFalling;
    private Picture ninjaBeforeSlash;
    private Picture ninjaAfterSlash;
    private SimpleGfxPosition pos;
    private SimpleGfxGrid grid;
    private Mouse mouse;
    private MouseController mouseController;
    private double currentX;
    private double currentY;
    private Timer animationTimer;
    private Timer gravityTimer;





    public Ninja(SimpleGfxPosition pos) {



        this.ninjaStanding = new Picture((double)pos.getCol(), (double)pos.getRow(), "Ninja/ninjaStanding_Scale_test1.png");
        this.ninjaBeforeSlash = new Picture((double)pos.getCol(),(double)pos.getRow(),"Ninja/ninjaStartSlash_scale_2.png");
        this.ninjaAfterSlash = new Picture((double)pos.getCol(),(double)pos.getRow(),"Ninja/ninjaFinalSlash_scale_2.png");
        this.ninjaFalling = new Picture((double)pos.getCol(),(double)pos.getRow(), "Ninja/ninjaFalling_Scale.png");

        this.ninjaStanding.draw();
        this.pos = pos;

        this.currentY = pos.getRow();
        this.currentX = pos.getCol();
    }



    public int getX() {
        return this.pos.getCol();
    }




    public int getY(){
        return this.pos.getRow();
    }



    public SimpleGfxPosition getPos(){
        return this.pos;
    }




    public void setGrid(SimpleGfxGrid grid) {
        this.grid = grid;
    }




    public void position(){

        int beforeCol = pos.getCol();
        int beforeRow = pos.getRow();

        int currentX = (int) this.currentX;
        int currentY = (int) this.currentY;


        pos.ninjaPosition(currentX, currentY);

        int afterCol = pos.getCol();
        int  afterRow = pos.getRow();

        ninjaBeforeSlash.translate(afterCol - beforeCol, afterRow - beforeRow);
        ninjaAfterSlash.translate(afterCol - beforeCol, afterRow - beforeRow);

    }




    public void slash(){

        ninjaStanding.delete();


        TimerTask timerTaskToDeleteBeforeSlash = new TimerTask() {
            @Override
            public void run() {
                ninjaBeforeSlash.delete();
            }
        };


        TimerTask timerTaskToDrawAfterSlash = new TimerTask() {
            @Override
            public void run() {
                ninjaAfterSlash.draw();
            }
        };

        TimerTask timerTaskToDeleteAfterSlash = new TimerTask() {
            @Override
            public void run() {

                ninjaAfterSlash.delete();
            }
        };

        TimerTask timerTaskToDrawBeforeSlash = new TimerTask() {
            @Override
            public void run() {
                ninjaBeforeSlash.draw();
            }
        };

        this.animationTimer  = new Timer();

        animationTimer.schedule(timerTaskToDeleteBeforeSlash, 0);

        animationTimer.schedule(timerTaskToDrawAfterSlash, 0);

        animationTimer.schedule(timerTaskToDeleteAfterSlash, 250);

        animationTimer.schedule(timerTaskToDrawBeforeSlash, 250);



    }



    /*public void startGravity(){

        TimerTask timerTaskToDeleteBeforeSlash = new TimerTask() {
            @Override
            public void run() {
                ninjaBeforeSlash.delete();
            }
        };

        TimerTask timerTaskToDrawFallling = new TimerTask() {
            @Override
            public void run() {
                ninjaFalling.draw();
            }
        };

        TimerTask timertaskToStartGravity = new TimerTask() {
            @Override
            public void run() {
                gravity();
            }
        };

        this.gravityTimer = new Timer();

        gravityTimer.schedule(timerTaskToDeleteBeforeSlash, 250);

        gravityTimer.schedule(timerTaskToDrawFallling, 250);

        gravityTimer.schedule(timertaskToStartGravity, 250);



    }*/



    /*private void gravity(){

        for(int i = 0; i < 3; ++i) {


            int beforeCol = pos.getCol();
            int beforeRow = pos.getRow();


            pos.ninjaImplementGravity(1);

            int afterCol = pos.getCol();
            int  afterRow = pos.getRow();

            ninjaFalling.translate(afterCol - beforeCol, afterRow -beforeRow);

        }
    }*/



    public void ninjaStand(){

        ninjaBeforeSlash.delete();
        ninjaStanding.draw();
    }




    public void setmouseController(MouseController mouseController){
        this.mouseController = mouseController;
    }



    public void setPos(double x, double y) {
        this.currentX = x;
        this.currentY = y;
    }
}
